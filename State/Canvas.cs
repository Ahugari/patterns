﻿namespace Patterns.State
{
    public class Canvas
    {
        public ITool Tool { get; set; }

        public Canvas(ITool toolType)
        {
            Tool = toolType;
        }

        public void OnMouseDown()
        {
            Tool.OnMouseDown();
        }

        public void OnMouseUp()
        {
            Tool.OnMouseUp();
        }
    }

    public interface ITool
    {
        public void OnMouseDown();

        public void OnMouseUp();
    }

    public class Brush : ITool
    {
        public void OnMouseDown()
        {
            System.Console.WriteLine("Brush Icon");
        }

        public void OnMouseUp()
        {
            System.Console.WriteLine("Draw a line.");
        }
    }

    public class Eraser : ITool
    {
        public void OnMouseDown()
        {
            System.Console.WriteLine("Eraser Icon");
        }

        public void OnMouseUp()
        {
            System.Console.WriteLine("Erase element on canvas.");
        }
    }

    public class Selector : ITool
    {
        public void OnMouseDown()
        {
            System.Console.WriteLine("Selector Icon");
        }

        public void OnMouseUp()
        {
            System.Console.WriteLine("Draw a rectangle.");
        }
    }
}