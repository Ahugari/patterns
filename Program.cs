﻿using Patterns.Momento;
using System;

namespace Patterns
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //ConfigureEditor();
            //ConfigureDocument();
        }

        private static void ConfigureEditor()
        {
            var editor = new Editor();
            var editorHistory = new History<EditorState>();

            editor.Content = "a";
            editorHistory.Push(editor.CreateState());

            editor.Content = "b";
            editorHistory.Push(editor.CreateState());

            editor.Content = "c";
            editor.RevertState(editorHistory.Pop());
            editor.RevertState(editorHistory.Pop());

            Console.WriteLine(editor.Content);
        }

        private static void ConfigureDocument()
        {
            var document = new Document();
            var documentHistory = new History<DocumentState>();

            document.Content = "con";
            documentHistory.Push(document.SaveState());

            document.FontSize = 22;
            documentHistory.Push(document.SaveState());

            document.FontName = "Calibri";
            document.Content = "content";
            document.FontSize = 16;
            documentHistory.Push(document.SaveState());

            document.Content = "contents";
            document.RevertState(documentHistory.Pop());
            document.RevertState(documentHistory.Pop());

            Console.WriteLine(document.ToString());
        }
    }
}