﻿using System.Collections.Generic;

namespace Patterns.Momento
{
    public class Editor
    {
        public string Content { get; set; }

        public EditorState CreateState()
        {
            return new EditorState(this);
        }

        public void RevertState(EditorState state)
        {
            Content = state.Content;
        }
    }

    public class EditorState
    {
        public string Content { get; set; }

        public EditorState(Editor editor)
        {
            Content = editor.Content;
        }
    }

    public class History<T> where T : class
    {
        private readonly Stack<T> _stateList = new Stack<T>();

        public void Push(T state) => _stateList.Push(state);

        public T Pop() => _stateList.Pop();
    }
}