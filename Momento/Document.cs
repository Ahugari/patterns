﻿namespace Patterns.Momento
{
    public class Document
    {
        public string Content { get; set; }
        public string FontName { get; set; }
        public int FontSize { get; set; }

        public DocumentState SaveState()
        {
            return new DocumentState(this);
        }

        public void RevertState(DocumentState document)
        {
            Content = document.Content;
            FontSize = document.FontSize;
            FontName = document.FontName;
        }

        public override string ToString()
        {
            return "Document{" +
                    "content='" + Content + '\'' +
                    ", fontName='" + FontName + '\'' +
                    ", fontSize=" + FontSize +
                    '}';
        }
    }

    public class DocumentState
    {
        public string Content { get; set; }
        public string FontName { get; set; }
        public int FontSize { get; set; }

        public DocumentState(Document document)
        {
            Content = document.Content;
            FontSize = document.FontSize;
            FontName = document.FontName;
        }
    }
}